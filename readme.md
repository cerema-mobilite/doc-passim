Annuaire PASSIM : documentation du projet
=========================================

**PASSIM** est un annuaire des services et offres de transport en France, géré par le CEREMA et soutenu par le Ministère du Développement Durable.
Pour accéder à la doc, [cliquez ici pour accéder au wiki](https://gitlab.com/cerema-mobilite/doc-passim/wikis/home).
PASSIM est construit uniquement avec des logiciels libres, dont le code est ici:
https://gitlab.easter-eggs.com/groups/cst-mat